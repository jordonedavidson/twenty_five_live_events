<?php

namespace Drupal\twenty_five_live_events\Tests\Unit;

use Drupal\Tests\UnitTestCase;
use Drupal\twenty_five_live_events\R25LiveConnection;

/**
 * Unit Tests for the R25LiveConnection module.
 *
 * @group twenty_five_live_events
 */
class R25LiveConnectionTest extends UnitTestCase {

  /**
   * Set the default theme for use in tests.
   *
   * @var string
   *
   * @see https://www.drupal.org/node/3083055.
   */
  protected $defaultTheme = 'stable';

  /**
   * Test the setStatus method.
   */
  public function testSetStatus() {
    $api = new R25LiveConnection();
    $expected = [
      'error' => TRUE,
      'code'  => 401,
      'message' => 'Unauthorized',
    ];

    $api->setStatus(TRUE, 401, 'Unauthorized');

    $this->assertEquals($expected['error'], $api->getStatus('error'));
    $this->assertEquals($expected['code'], $api->getStatus('code'));
    $this->assertEquals($expected['message'], $api->getStatus('message'));

  }

}
