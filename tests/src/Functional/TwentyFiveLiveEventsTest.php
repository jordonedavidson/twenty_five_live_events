<?php

namespace Drupal\twenty_five_live_events\Tests\Functional;

use Drupal\Tests\BrowserTestBase;

/**
 * Functional Tests for the 25Live Events module.
 *
 * @group twenty_five_live_events
 */
class TwentyFiveLiveEventsTest extends BrowserTestBase {
  /**
   * Modules to install.
   *
   * @var array
   */
  protected static $modules = ['twenty_five_live_events'];

  /**
   * Set the default theme for use in tests.
   *
   * @var string
   *
   * @see https://www.drupal.org/node/3083055.
   */
  protected $defaultTheme = 'stable';

  /**
   * A simple user.
   *
   * @var \Drupal\user\Entity\User
   */
  private $user;

  /**
   * Perform inital setuip taxks that run before every test method.
   */
  public function setUp() {
    parent::setUp();

    $this->user = $this->drupalCreateUser(
      [
        'administer site configuration',
      ],
      'bob',
      TRUE
    );
  }

  /**
   * Tests the config form.
   */
  public function testConfigForm() {
    // Login.
    $this->drupalLogin($this->user);

    // Access config page.
    $this->drupalGet('/admin/config/system/twenty-five-live-events');
    $this->assertSession()->statusCodeEquals(200);
  }

}
