<?php

namespace Drupal\twenty_five_live_events\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\twenty_five_live_events\R25LiveEvents;

/**
 * Returns responses for 25 Live Events routes.
 */
class TwentyFiveLiveEventsController extends ControllerBase {
  /**
   * Instance of R25LiveEvents.
   *
   * @var Drupal\twenty_five_live_events\R25LiveEvents
   */
  private $api;

  /**
   * Constructor.
   */
  public function __construct() {
    $this->api = new R25LiveEvents();
  }

  /**
   * Builds the response.
   */
  public function build() {
    $eventCategories = $this->api->getEventCategories();

    $build['content'] = [
      '#type' => 'item',
      '#markup' => '<pre>' . print_r($eventCategories, TRUE) . '</pre>',
    ];

    return $build;
  }

  /**
   * Display an Event.
   */
  public function event(int $event_id) {
    $content = [
      '#event' => [],
      '#theme' => 'event',
    ];

    $content['#event'] = $this->api->getEvent($event_id);

    \Drupal::logger('twenty_five_live_events')->info(print_r($content, TRUE));
    return $content;
  }

  /**
   * Get the next upcoming events and show them.
   */
  public function index() {
    $eventTypes = array_filter($this->config('twenty_five_live_events.settings')->get('r25types'));
    $eventCategories = array_filter($this->config('twenty_five_live_events.settings')->get('r25categories'));

    $content = [
      '#hasEvents' => FALSE,
      '#events' => [],
      '#theme' => 'index',
    ];

    // Set up the parmeters.
    $start = new \DateTime();
    $end = new \DateTime();
    $end->add(new \DateInterval('P40D'));
    $parameters = [
      'start_dt' => $start->format('Ymd'),
      'end_dt' => $end->format('Ymd'),
      'event_state' => 2,
      'node_type' => 'E',
      'scope' => 'extended',
      'category_id' => implode('+', $eventCategories),
      // 'event_type_id' => implode('+', $eventTypes),
    ];

    // Get the events.
    $upcomingEvents = $this->api->getEventsList($parameters);

    if (is_array($upcomingEvents)) {
      $content['#hasEvents'] = TRUE;
      $content['#events'] = array_slice($upcomingEvents, 0, 10);
    }

    return $content;
  }

}
