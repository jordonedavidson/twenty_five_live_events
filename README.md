# 25Live Events Module

> This module is in development and should not be included on a production __Drupal__ site.
## Connects to the 25Live events API to read and display events locally, avoiding the horrendous spud interface

## Releases
* 0.9.2 - Continued devlopment, now with testing and some event updates

* 0.9.0 - Proof of concept release. Connects to the API and lists some events in a Block.

## ToDos

* [ ] Better error control in the API Connection Class (R25LiveConnection)
* [x] Output on the main events page.
* [x] Handle an event id input on the events page to load that event.
* [x] Provide admin interface for filtering event categories.
* [x] Handle display of all events for a given organization.
* [ ] Calendar interface.
* [ ] Show all events of a given type
* [ ] Show all events for a given location
* [ ] Load more events on the main page

## Testing

### Setup

The module needs to be installed in a valid Drupal instance which has a valid phpunit installation.

All drupal phpunit tests are run from the `web/core` directory.
If a `phpunit.xml` configuration file does not live there the `-c <path to phpunit.xml>` option must be used in the command.

### Running the tests

From the `web/core` directory run:

```console
../../vendor/bin/phpunit -c ../../phpunit.xml ../modules/custom/twenty_five_live_events/
```
